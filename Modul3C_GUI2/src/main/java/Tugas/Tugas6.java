/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tugas;

import java.awt.PopupMenu;
import java.awt.TextArea;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author asus
 */
public class Tugas6 extends JDialog {

    public Tugas6() {
        textScrool();
        comboBox();
        setTitle("ComboBoxDemo");
        setSize(500, 300);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setVisible(true);
    }

    public void comboBox() {
        String[] negara = {"English", "Canada", "Indonesia", "Malaysia"};
        this.setLayout(null);
        JComboBox comboBox_negara = new JComboBox(negara);
        comboBox_negara.setBounds(1, 1, 400, 20);
        this.add(comboBox_negara);

        ImageIcon canada = new ImageIcon("images/canada.png");
        JLabel labelGambar = new JLabel(canada);
        labelGambar.setBounds(2, 10, 250, 200);
        this.add(labelGambar);

        JLabel labelNamaGambar = new JLabel("Canada");
        labelNamaGambar.setBounds(2, 10, 250, 200);
        this.add(labelGambar);
    }

    public void textScrool() {
        JTextArea textArea_deskripsi = new JTextArea("Negara Canada");
        textArea_deskripsi.setBounds(300, 40, 160, 200);
        JScrollPane scroll = new JScrollPane(textArea_deskripsi, 
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroll.setBounds(300, 40, 160, 200);
        this.add(scroll);
    }

    public static void main(String[] args) {
        Tugas6 test = new Tugas6();

    }
}
