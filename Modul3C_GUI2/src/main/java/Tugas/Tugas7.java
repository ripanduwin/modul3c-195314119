/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tugas;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 *
 * @author asus
 */
public class Tugas7 extends JDialog {
    
    String [] data = {"Canada", "Cina", "Denmark", "France","Germany",
        "India","Norway","United Kindom","United States Of America"};
    public static void main(String[] args) {
        Tugas7 dialog = new Tugas7();
        dialog.setVisible(true);
    }
    
    public Tugas7(){
        setSize(500,250);
        setTitle("ListDemo");
        setLocation(150,250);
        
        this.setLayout(new GridLayout(1,2));
        
        JPanel panel1 = new JPanel();
        JList list = new JList(data);
        panel1.add(new JScrollPane(list));
        JPanel panel2 = new JPanel(new FlowLayout());
        JLabel label1 = new JLabel();
        ImageIcon canada = new ImageIcon("images/canada.png");
        label1.setIcon(canada);
        JLabel label2 = new JLabel();
        ImageIcon indo = new ImageIcon("images/indo.png");
        label2.setIcon(indo);
        JLabel label3 = new JLabel();
        ImageIcon german = new ImageIcon("images/german.png");
        label3.setIcon(german);
        JLabel label4 = new JLabel();
        ImageIcon india = new ImageIcon("images/india.png");
        label4.setIcon(india);
        JLabel label5 = new JLabel();
        ImageIcon united = new ImageIcon("images/united.png");
        label5.setIcon(united);
        JLabel label6 = new JLabel();
        ImageIcon america = new ImageIcon("images/america.png");
        label6.setIcon(america);
        panel2.add(label1);
        panel2.add(label2);
        panel2.add(label3);
        panel2.add(label4);
        panel2.add(label5);
        panel2.add(label6);
        this.add(panel1);
        this.add(panel2);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    }
}
