/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tugas;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author asus
 */
public class Tugas1 extends JFrame implements ActionListener{
    
    private JMenuBar menuBar;
    
    private JMenuItem file_Tampil1;
    private JMenuItem file_Tampil2;
    
    private JMenu file;
    private JMenu edit;
    
    public static void main(String[] args) {
        JFrame frame = new Tugas1();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    public Tugas1(){
        Container konten = getContentPane();
        setSize(500,300);
        setResizable(true);
        setTitle("Frame Pertama");
        konten.setBackground(Color.pink);
        
        menuBar = new JMenuBar();
        
        file = new JMenu("File");
        edit = new JMenu("Edit");
        
        menuBar.add(file);
        menuBar.add(edit);
        this.setJMenuBar(menuBar);
        
        file_Tampil1 = new JMenuItem("Tampil 1");
        file_Tampil2 = new JMenuItem("Tampil 2");
        
        file.add(file_Tampil1);
        file.add(file_Tampil2);
        
        file_Tampil1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Container tampil1 = getContentPane();
                tampil1.setBackground(Color.white);
                tampil1.setVisible(true);
            }
        });
        
        file_Tampil2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Container tampil2 = getContentPane();
                tampil2.setBackground(Color.black);
                tampil2.setVisible(true);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      
    }
}
