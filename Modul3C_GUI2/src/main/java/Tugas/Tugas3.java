/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tugas;

import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;

/**
 *
 * @author asus
 */
public class Tugas3 extends JDialog{
    
    private JLabel label_image, label_text;
    public static void main(String[] args) {
        JDialog dialog = new Tugas3();
        dialog.setVisible(true);
    }
    
    public Tugas3(){
        this.setTitle("Text and Icon Label");
        this.setSize(300, 200);
        this.setLayout(null);
        
        ImageIcon icon = new ImageIcon("images/Grapes.jpg");
        label_image = new JLabel();
        label_image.setIcon(icon);
        label_image.setBounds(40, 0, 200, 120);
        this.add(label_image);
        
        label_text = new JLabel("Grapes");
        label_text.setBounds(120,120,80,40);
        this.add(label_text);
        
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    }
}
