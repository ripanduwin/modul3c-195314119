/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tugas;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JDialog;

/**
 *
 * @author asus
 */
public class Tugas2 extends JDialog{
    
    private JButton button1,  button2, button3;
    public static void main(String[] args) {
        JDialog dialog = new Tugas2();
        dialog.setVisible(true);
    }
    
    public Tugas2(){
        Container konten = getContentPane();
        setSize(300,200);
        setResizable(true);
        setTitle("Button Test");
        konten.setLayout(new FlowLayout());
        
        button1 = new JButton("Yellow");
        button2 = new JButton("Blue");
        button3 = new JButton("Red");
        
        konten.add(button1);
        konten.add(button2);
        konten.add(button3);
        
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE); 
    }
}
