/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tugas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JTextArea;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

/**
 *
 * @author asus
 */
public class Tugas5 extends JDialog{
    
    private final Container konten;
    private final JButton button_left;
    private final JButton button_right;
    private final JCheckBox centered;
    private final JCheckBox bold;
    private final JCheckBox italic;
    private final JPanel panel1;
    private final JPanel panel2;
    private final JPanel panel3;
    private final JPanel panel4;
    private final JTextArea text;
    private final JRadioButton red;
    private final JRadioButton green;
    private final JRadioButton blue;
    
    public static void main(String[] args) {
        JDialog dialog = new Tugas5();
        dialog.setVisible(true);
    }
    
    public Tugas5(){
        setSize(300,150);
        setTitle("CheckBoxDemo");
        
        konten = getContentPane();
        konten.setLayout(new BorderLayout());
        
        panel1 = new JPanel();
        panel1.setBackground(Color.white);
        text = new JTextArea("Welcome to Java");
        panel1.add(text);
        
        panel2 = new JPanel(new GridLayout(3,1));
        centered = new JCheckBox("Centered");
        panel2.add(centered);
        bold = new JCheckBox("Bold");
        panel2.add(bold);
        italic = new JCheckBox("Italic");
        panel2.add(italic);
        
        panel3 = new JPanel();
        button_left = new JButton("Left");
        panel3.add(button_left);
        button_right = new JButton("Right");
        panel3.add(button_right);
        
        panel4 = new JPanel(new GridLayout(3,1));
        red = new JRadioButton("Red");
        panel4.add(red);
        green = new JRadioButton("Green");
        panel4.add(green);
        blue = new JRadioButton("Blue");
        panel4.add(blue);
        
        konten.add(panel1, BorderLayout.CENTER);
        konten.add(panel2, BorderLayout.EAST);
        konten.add(panel3, BorderLayout.SOUTH);
        konten.add(panel4, BorderLayout.WEST);
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
}
