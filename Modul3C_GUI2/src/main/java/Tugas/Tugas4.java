/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tugas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JTextArea;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *
 * @author asus
 */
public class Tugas4 extends JDialog{
    
    private final Container konten;
    private final JButton button_left;
    private final JButton button_right;
    private final JCheckBox centered;
    private final JCheckBox bold;
    private final JCheckBox italic;
    private final JPanel panel1;
    private final JPanel panel2;
    private final JPanel panel3;
    private final JTextArea text;
    
    public static void main(String[] args) {
        JDialog dialog = new Tugas4();
        dialog.setVisible(true);
    }
    
    public Tugas4(){
        setSize(300,150);
        setTitle("CheckBoxDemo");
        
        konten = getContentPane();
        konten.setLayout(new BorderLayout());
        
        panel1 = new JPanel();
        panel1.setBackground(Color.white);
        text = new JTextArea("Welcome to Java");
        panel1.add(text);
        
        panel2 = new JPanel(new GridLayout(3,1));
        centered = new JCheckBox("Centered");
        panel2.add(centered);
        bold = new JCheckBox("Bold");
        panel2.add(bold);
        italic = new JCheckBox("Italic");
        panel2.add(italic);
        
        panel3 = new JPanel();
        button_left = new JButton("Left");
        panel3.add(button_left);
        button_right = new JButton("Right");
        panel3.add(button_right);
        
        konten.add(panel1, BorderLayout.CENTER);
        konten.add(panel2, BorderLayout.EAST);
        konten.add(panel3, BorderLayout.SOUTH);
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
//    
//    public JPanel Tugas4(){
//        JPanel utama = new JPanel();
//        
//        utama.setLayout(new BorderLayout());
//        
//        JPanel panel1 = new JPanel();
//        JLabel label = new JLabel("Welcome to Java");
//        panel1.add(label);
//        
//        JPanel panel2 = new JPanel(new GridLayout(3,1));
//        JCheckBox centered = new JCheckBox("Centered");
//        panel2.add(centered);
//        JCheckBox bold = new JCheckBox("Bold");
//        panel2.add(bold);
//        JCheckBox italic = new JCheckBox("Italic");
//        panel2.add(italic);
//        
//        JPanel panel3 = new JPanel();
//        JButton button_left = new JButton("Left");
//        panel3.add(button_left);
//        JButton button_right = new JButton("Right");
//        panel3.add(button_right);
//        
//        utama.add(panel1, BorderLayout.WEST);
//        utama.add(panel2, BorderLayout.EAST);
//        utama.add(panel3, BorderLayout.SOUTH);
//        
//        utama.setOpaque(true);
//        
//        return utama;
//    }
//    
//    public static void tampil(){
//        JDialog.setDefaultLookAndFeelDecorated(true);
//        JDialog dialog = new JDialog();
//        dialog.setTitle("CheckBoxDemo");
//        
//        Tugas4 cetak = new Tugas4();
//        dialog.setContentPane(cetak.Tugas4());
//        
//        dialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
//        dialog.setSize(300,150);
//        dialog.setVisible(true);
//    }
//    public static void main(String[] args) {
//        SwingUtilities.invokeLater(new Runnable(){
//            public void run(){
//                tampil();
//            }
//        });
//    }
}
